%% Initialisation agent par renforcement
ActionInfo = rlNumericSpec([1 1]);
ActionInfo.Name = 'Action controlX';
ActionInfo.LowerLimit= -Vsat;
ActionInfo.UpperLimit= Vsat;

ObservationInfo = rlNumericSpec([5 1]);
ObservationInfo.Name = 'etats controlX';
ObservationInfo.Description = 'x, sin(psi), cos(psi), dx, dpsi';

mdl='modele_simulink';

env = rlSimulinkEnv(mdl,'modele_simulink/RL Agent', ObservationInfo, ActionInfo);

env.ResetFcn = @(in)localResetFcn(in, psip_max, psi_i, x_max);

agentOpts = rlSACAgentOptions(...
    'SampleTime', Ts_agent,...
    'NumStepsToLookAhead', 10, ... % Nombre de pas de temps à regarder dans le futur pour faire l'estimation de Q (TD-Lambda)
    'DiscountFactor',1-Ts_agent,... % Facteur d'actualisation
    'TargetSmoothFactor', 0.05, ... % Paramètre pour ne pas mettre trop vite à jour le critic
    'MiniBatchSize', 128,... % Taille du batch size pour chaque descente de gradient
    'ExperienceBufferLength', 1500,... # Taille de la mémoire des actions et récompenses passées, on échantillonne dedant pour créer le minibatch (ok)
    'NumWarmStartSteps', 1/Ts_agent,  ... % Nombre de pas à effectuer avant de commencer l'apprentissage
    'CriticUpdateFrequency', 1,  ... % Entraine le critic tout les 1 pas de temps
    'PolicyUpdateFrequency', 1); % Entraine l'actor tout les 1 pas de temps

% [actor,critics] = creation_actor_critics(ObservationInfo,ActionInfo);
% agent = rlSACAgent(actor,critics, agentOpts);
agent = rlSACAgent(ObservationInfo,ActionInfo, agentOpts); % Intencie un agent SAC avec les paramètres par défault de Matlab
% agent = rlPPOAgent(ObservationInfo,ActionInfo);

%% Fonction reset de l'environnement
function in = localResetFcn(in, psip_max, psi_i, x_max)
    x_initial=0.3*x_max*2*(rand-0.5); % Initialisation de la position linéaire (entre [-30% +30%] du max)
    psi_initial=pi*(5/180) * 2*(rand-0.5) + psi_i; % Initialisation de la position angulaire (entre [-5° +5°])
    xp_initial=0;
    psip_initial = 0.05*psip_max*2*(rand-0.5); % Initialisation de la vitesse angulaire (entre [-5% +5%] du max)

    in = in.setVariable('x_init',x_initial);
    in = in.setVariable('psi_i',psi_initial);
    in = in.setVariable('xp_init',xp_initial);
    in = in.setVariable('psip_init',psip_initial);

end