clear all
initialisation_constantes % Initialise les constantes du modèle
initialisation_constantes_capteurs  % Initialyse le capteur

psi_max = 15*pi/180; % Angle max (radian)
psi_i = 0; % Position initiale du pendule

initialisation_env_agent % Initialise l'environnement et l'agent par renforcement