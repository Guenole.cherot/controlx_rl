function [actor,critics] = creation_actor_critics(ObservationInfo,ActionInfo)


%%%%%%%%%%%%%%%%%%%%%%% Création de l'actor %%%%%%%%%%%%%%%%%%%%%%%
sequencial = [featureInputLayer(ObservationInfo.Dimension(1), 'Normalization','none','Name','myobs')
    fullyConnectedLayer(128,'Name','fc1')
    reluLayer('Name','relu_1')
    fullyConnectedLayer(200,'Name','fc2')
    reluLayer('Name','relu_2')
    fullyConnectedLayer(1,'Name','fc3')];
mu = [tanhLayer('Name','tanh');
    scalingLayer('Name','scale','Scale',ActionInfo.UpperLimit)];
log_std = [softplusLayer('Name', 'splus')];
concat = concatenationLayer(4,2,'Name','concat');

net_actor = layerGraph();
net_actor = addLayers(net_actor, sequencial);
net_actor = addLayers(net_actor, mu);
net_actor = addLayers(net_actor, log_std);
net_actor = addLayers(net_actor, concat);
net_actor = connectLayers(net_actor, 'fc3', 'tanh');
net_actor = connectLayers(net_actor, 'fc3', 'splus');
net_actor = connectLayers(net_actor, 'scale', 'concat/in1');
net_actor = connectLayers(net_actor, 'splus', 'concat/in2');
% plot(net_actor)

actorOpts = rlRepresentationOptions('LearnRate',5e-4,'UseDevice', "gpu", "GradientThreshold", 1);
actor = rlStochasticActorRepresentation(net_actor,ObservationInfo,ActionInfo, ...
    'Observation',{'myobs'}, actorOpts);

%%%%%%%%%%%%%%%%%%%%%%% Création du Critic 1 %%%%%%%%%%%%%%%%%%%%%%%

obsPath = featureInputLayer(ObservationInfo.Dimension(1), 'Normalization','none','Name','myobs');
sequential1 = [fullyConnectedLayer(128,'Name','fc1')
    reluLayer('Name','relu_1')
    fullyConnectedLayer(200,'Name','fc2')];
actPath = featureInputLayer(1, 'Normalization','none','Name','myact');
sequential2 = [fullyConnectedLayer(200,'Name','fc3')];
add = [additionLayer(2,'Name','add_1')
    fullyConnectedLayer(1,'Name','fc4')];

net_critic1 = layerGraph();
net_critic1 = addLayers(net_critic1, obsPath);
net_critic1 = addLayers(net_critic1, actPath);
net_critic1 = addLayers(net_critic1, sequential1);
net_critic1 = addLayers(net_critic1, sequential2);
net_critic1 = addLayers(net_critic1, add);
net_critic1 = connectLayers(net_critic1, 'myobs', 'fc1');
net_critic1 = connectLayers(net_critic1, 'myact', 'fc3');
net_critic1 = connectLayers(net_critic1, 'fc2', 'add_1/in1');
net_critic1 = connectLayers(net_critic1, 'fc3', 'add_1/in2');
plot(net_critic1)

criticOpts = rlRepresentationOptions('LearnRate',1e-3,'UseDevice', "gpu", "GradientThreshold", 1);
critic1 = rlQValueRepresentation(net_critic1,ObservationInfo,ActionInfo, ...
    'Observation',{'myobs'},'Action',{'myact'}, criticOpts);

%%%%%%%%%%%%%%%%%%%%%%% Création du Critic 2 %%%%%%%%%%%%%%%%%%%%%%%
% Même caractéristiques
obsPath = featureInputLayer(ObservationInfo.Dimension(1), 'Normalization','none','Name','myobs');
sequential1 = [fullyConnectedLayer(128,'Name','fc1')
    reluLayer('Name','relu_1')
    fullyConnectedLayer(200,'Name','fc2')];
actPath = featureInputLayer(1, 'Normalization','none','Name','myact');
sequential2 = [fullyConnectedLayer(200,'Name','fc3')];
add = [additionLayer(2,'Name','add_1')
    fullyConnectedLayer(1,'Name','fc4')];

net_critic2 = layerGraph();
net_critic2 = addLayers(net_critic2, obsPath);
net_critic2 = addLayers(net_critic2, actPath);
net_critic2 = addLayers(net_critic2, sequential1);
net_critic2 = addLayers(net_critic2, sequential2);
net_critic2 = addLayers(net_critic2, add);
net_critic2 = connectLayers(net_critic2, 'myobs', 'fc1');
net_critic2 = connectLayers(net_critic2, 'myact', 'fc3');
net_critic2 = connectLayers(net_critic2, 'fc2', 'add_1/in1');
net_critic2 = connectLayers(net_critic2, 'fc3', 'add_1/in2');
plot(net_critic2)

criticOpts = rlRepresentationOptions('LearnRate',1e-3, 'UseDevice', "gpu", "GradientThreshold", 1);
critic2 = rlQValueRepresentation(net_critic2,ObservationInfo,ActionInfo, ...
    'Observation',{'myobs'},'Action',{'myact'}, criticOpts);

critics = [critic1, critic2];
end