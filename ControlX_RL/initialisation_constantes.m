%% Contraintes
x_max = 0.15; % Déplacement max (m)
x_lim = 0.12; % Déplacement à partir duquel on donne une pénalité
xp_max = 1.1; % Vitesse max du système (m/s)
psip_max = 5*2*pi; % Vitesse angulaire max du système (rad/s)
Vsat = 10; % Tension de commande maximal (en Volt)

%% Paramètre modifiables
x_init= 0;
xp_init= 0;
psip_init=0;
Ts = 1e-3;
Ts_agent = 10*Ts;
delais = Ts_agent/Ts;
% Type de récompense utilisé.
% 1 : constante égale à un.
% 2 : Bi-niveau un niveau facile pour inciter à redresser, un niveau difficile pour inciter à stabiliser.
% 3 : Proportionnelle à l'énergie potentiel de pensenteur du système
type_recompense = 2; 
penalisation = 0; % Si égale à 0 : ne pénalise pas les changements de consignes trop brusques. Si 1 oui (voir bloc récompense dans simulink)
connecte_au_controlX = 0; % 0 si on souhaite connecter l'agent au control'X, 1 sinon

%% Paramètres physique
g=9.81; % Constante de pesenteur
B=4;
kc = 0.21; % Constante de couple du moteur (N.m/A)
ke = 0.208; % Constante de force contre électromotrice (V/(rad/s))
r = 5.1; % Résistance de l'induit (Ohms)
L = 3.2e-3; % Inductance de l'induit(H)

R=155/(2*pi)/1000; % Rayon de la poulie (m) // 155mm : avance par tour de poulie crantée
red=3;    % Reducteur : il s'agit d'une réduction

%chariot
Ffrott = 28; % Fottements sec de tout l'équipage mobile ramené sur le chariot (N)
fv = 20; % Coefficient de frottements visqueux de tout l'équipage mobile ramenés sur le chariot : N/(m/s)
meq = 3.2; % Masse équivalente de tout l'équipage mobile ramenée sur le chariot (kg)
Jeq = meq*(R/red)^2; % Moment d'inertie équivalent

%pendule
Cf_pendule=3e-3; %Couple de frottement sec dans le pivot du pendule
fw_pendule=1e-4; % Coefficient de couple de frottement visqueux dans le pivot du pendule
m = 122.9e-3;% masse totale pendule (kg)
l = 112e-3; % distance du centre de gravité du pendule à l'axe de rotation (m)
I_pendule = 1.557e-3; % moment d'inertie par rapport à l'axe de rotation du pendule (kg.m2)
l = 105e-3; % distance du centre de gravité du pendule à l'axe de rotation (m)
I = I_pendule;

fw = fv*R^2/red^2; % Coefficient de frottements visqueux de tout l'équipage mobile ramenés sur le moteur : N.m/(rad/s)

kprim_c = kc*red/R;
kprim_e = ke*red/R;

kprim = (kprim_c+kprim_e)/2;

action_precedente=0;