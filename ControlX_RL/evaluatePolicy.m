function action1 = evaluatePolicy(observation1)
%#codegen

% Reinforcement Learning Toolbox
% Generated on: 18-Feb-2022 14:22:53

numAction = 1;
actionScaleLocal = reshape([10],[],1);
actionBiasLocal = reshape([0],[],1);
% Get the actions mean and standard deviation
meanAndStd = localEvaluate(observation1);
actionMean = meanAndStd(1:numAction);
actionStd = meanAndStd(numAction+1:end);

% Sample action from mean and standard deviation
action1 = actionMean + actionStd .* randn(size(actionMean),'like',actionMean);
action1 = reshape(action1,[],1);

% Ranges of all action dimensiosn are bounded.
action1 = tanh(action1).*actionScaleLocal + actionBiasLocal;
action1 = reshape(action1,[1  1]);
end
%% Local Functions
function meanAndStd = localEvaluate(observation1)
persistent policy
if isempty(policy)
	policy = coder.loadDeepLearningNetwork('agentData.mat','policy');
end
observation1 = observation1(:)';
meanAndStd = predict(policy, observation1);
end