clear all
initialisation_constantes % Initialise les constantes du modèle
initialisation_constantes_capteurs  % Initialyse le capteur

psi_max = inf; % Angle max (radian), pas de contrainte sur l'angle max
psi_i = pi; % Position initiale du pendule en bas

initialisation_env_agent % Initialise l'environnement et l'agent par renforcement