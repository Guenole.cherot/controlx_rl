sens_psi=1; % choisir entre -1 et 1 pour définir le sens positif de psi
C_pend=-2048*4/(2*pi); %gain codeur incremental pendule en inc/rad
D_pend=1/C_pend; %gain codeur (rad/inc)

C=1000*4/(2*pi); % Points par radian de rotation de moteur, décodé en *4
D=R/(red*C)*1000; % Gain adaptateur en mm/inc, pour compatibilité avec certains fichiers liés au chariot seul
D_chariot=R/(red*C); % en m/inc
gain_vitesse_tr_min=1000/6.35; % tr/min de l'arbre moteur
gain_vitesse_m_s=gain_vitesse_tr_min/60*2*pi*R/3; % en m/s (vitesse chariot)

Tf=0.008; %Constante de temps pour filtrage ou pseudo dérivation de certains signaux (s)
dt = Ts;