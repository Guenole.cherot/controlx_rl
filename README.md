# Introduction à l'apprentissage par renforcement
Dans ce dépot :
1. Relever et stabiliser un pendule simple à l'aide de Matlab. Application sur le sytème Control'X.
2. Application PyGame permettant de se mettre dans la peau d'un agent par renforcement.
3. Introduction à l'utilisation de deux librairies d'apprentissage par renforcement (*Gym* et *Stable BaseLines3*) sur Python.

## Apprentissage par renforcement pour relever un pendule simple

### Modèle utilisé
Système Control'X produit par DMS : ([lien](https://www.dmseducation.eu/controlx-xml-350_367-1126.html)).
Axe linéaire hautement dynamique, le déplacement du chariot se fait grâce à un moteur à courant continu piloté en tension.
Le détail de l'activité pratique est détaillé dans le fichier : *Activité_pratique_ControlX.pdf*.

### Formalisation du problème
![Illustration du pendule](illustration.png)

- Espace d'action : Tension délivré au moteur +/- 10 V
- Espace d'observation
  - Position linéaire du chariot (x)
  - Position angulaire du pendule (sin(psi), cos(psi))
  - Vitesse linéaire du chariot (x point)
  - Vitesse angulaire du pendule (psi point)
- La simulation s'arrête si une des conditions suivante est atteinte
  - Le chariot est en bout de course (x max = 0.15m)
  - Le pendule dépasse l'angle max (voir "Comment utiliser ce code")
  - La vitesse du chariot est trop rapide (x point max = 1.1m/s)
  - La vitesse angulaire du pendule est trop importante (psi max = 10 pi rad/s)

### Comment utiliser ce code
Distribution : Matlab2021b

Pour lancer un apprentissage :
- Lancer le fichier "modele_simulink.slx"
- Initialiser les constantes avec un des boutons d'initialisation
  - "Position haute" : initialise la simulation avec le pendule en position haute, la simulation s'arrête quand le pendule dépasse +/- 15°
  - "Position basse" : initialise la simulation avec le pendule en position basse, pas de condition d'arrête sur l'angle
- Choisir le type de récompense (défini dans le bloc récompense dans l'environnement simulink)
  - Constante : Récompense de 1 à chaque pas de temps
  - Multi-objectif : Pondération des différent objectif (pendule en postion haute, centré, vitesse angulaire et linéaire nulles)
  - Autre : la récompense vaut cos(psi) l'angle du pendule dont l'origine est placé en haut.
- Choisir le type de pénalisation (Sans pénalisation, la commande est trop brusque et risque d'endommager le système sur le long terme)
  - Pas de pénalisation
  - Pénalisation en Commande^2 (Incite la commande à rester proche de 0)
- Entrainer l'agent en appuyant sur "Entrainer"


Fichier .m :
Ces fichiers sont appelés dans Simulink, ils ne doivent pas être modifiés.

## Petit Jeu sur Pygame
Ne necessite pas Matlab. Permet de comprendre le contexte dans lequel est utilisé l'apprentissage par renforcement par le biais d'un petit jeu.
Ne nécessite aucune installation. Déziper le fichier jeu et ouvrir "jeu.exe".

## Introduction à Gym et StableBaseLines3
Activité de travaux pratique sur un NoteBook autour de l'utilisation de *Gym* et *Stable BaseLines3*.
Le NoteBook est disponible sur ce dépot Git et directement en ligne via *Google Colab* ([lien](https://colab.research.google.com/drive/1AZh_FSSImncpOuy_vdGbewK6OyqksAme?usp=sharing))

*Gym* est une boîte à outils pour le développement et la comparaison d'algorithmes d'apprentissage par renforcement.
Aucune hypothèse n'est faite sur la structure de l'agent.
L'interface est compatible avec toute bibliothèque de calcul numérique.

*Stable BaseLines3* est un ensemble d'implémentations fiables d'algorithmes d'apprentissage par renforcement dans PyTorch.